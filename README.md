# My actionhero Project

_visit www.actionherojs.com for more information_

## To install:

(assuming you have [node](http://nodejs.org/), [TypeScript](https://www.typescriptlang.org/), and NPM installed)

`yarn install`

## To Run:

`yarn start`

## To Test:

`yarn test`
