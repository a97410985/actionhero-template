const dialect = "mysql";

module.exports = {
  development: {
    username: "root",
    password: "123456",
    database: "actionherotemplate_development",
    host: "127.0.0.1",
    // eslint-disable-next-line object-shorthand
    dialect: dialect,
    autoMigrate: true,
  },
  test: {
    username: "root",
    password: "123456",
    database: "actionherotemplate_test",
    host: "mysql",
    // eslint-disable-next-line object-shorthand
    dialect: dialect,
  },
  production: {
    username: "root",
    password: null,
    database: "actionherotemplate_production",
    host: "mysql",
    // eslint-disable-next-line object-shorthand
    dialect: dialect,
  },
};
