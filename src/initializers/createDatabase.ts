import { log, config } from "actionhero";
import { SequelizeInitializer } from "ah-sequelize-plugin/dist/initializers/sequelize";
import * as mysql from "mysql2/promise";

export class CreateDatabase extends SequelizeInitializer {
  constructor() {
    super();
    this.name = "createDatabase";
  }

  async initialize(): Promise<void> {
    await this.createDatabase();
    await super.initialize();
    log(`CreateDatabase start() finish`);
  }

  async createDatabase(): Promise<void> {
    const dbName = config.sequelize.database;
    const createDatabaseQueryString = `CREATE DATABASE IF NOT EXISTS ${config.sequelize.database};`;
    log(
      `mySQLConnection : mysql://${config.sequelize.username}:${config.sequelize.password}@${config.sequelize.host}:${config.sequelize.port}/${dbName}`
    );

    await mysql
      .createConnection({
        host: config.sequelize.host,
        port: config.sequelize.port,
        user: config.sequelize.username,
        password: config.sequelize.password,
      })
      .then((connection) => {
        connection.query(createDatabaseQueryString).then(() => {
          connection.end();
          log("Database create or successfully checked");
        });
      });
  }
}
